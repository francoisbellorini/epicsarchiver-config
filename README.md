# epicsarchiver-config

This repository is used to store all PVs to archive.
This is in **alpha stage** and only to evaluate the concept.


## Introduction

For each IOC, the list of PVs to archive should be saved in a file named `<ioc name>.archive`.
The file should be put under a directory named after the archiver appliance fully-qualified domain name.

Here is the list of current archiver appliances:

- **archiver-01.tn.esss.lu.se** for the LCR
- **archiver-02.tn.esss.lu.se** for TS2
- **ics-archiver11.tn.esss.lu.se** for testing

## Archive File format

The files shall be in CSV format (space separated) and include:
- PV name
- sampling period (optional). Set to 1 second by default.
- sampling method SCAN|MONITOR (optional). Set to MONITOR by default.

Here is an example:

```
# PV name  Sampling period  Sampling method
CrS-ACCP:CRYO-GT-34884:Val 30 monitor
# Comments are allowed
CrS-TICP:Cryo-TE-33483:Val 5.0 SCAN
CrS-TICP:Cryo-TT-21220:Val 10
CrS-TICP:Cryo-TE-31459B:Val
```

## Workflow

When pushing to master, the PVs are automatically added to the archiver.

The `process_archives.py` script looks at files that changed since last commit.
All the PVs from those files are sent to the proper appliance for archiving.

PV deletion and parameters change (sampling period or method) are currently not handled.

